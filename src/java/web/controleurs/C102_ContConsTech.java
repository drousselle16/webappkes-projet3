
package web.controleurs;

import dal.consultation.technicien.DaoTechnicien;
import entites.Technicien;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped

public class C102_ContConsTech {
    
    @Inject private DaoTechnicien dao;
    
    private Long       numtech;
    private Technicien    technicien;
    
    public void rechercher(){ technicien= dao.getLeTechnicien(numtech);}

    public DaoTechnicien getDao() {
        return dao;
    }

    public void setDao(DaoTechnicien dao) {
        this.dao = dao;
    }

    public Long getNumtech() {
        return numtech;
    }

    public void setNumtech(Long numtech) {
        this.numtech = numtech;
    }

    public Technicien getTechnicien() {
        return technicien;
    }

    public void setTechnicien(Technicien technicien) {
        this.technicien = technicien;
    }
    
    
}
