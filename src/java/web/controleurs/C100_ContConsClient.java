
package web.controleurs;

import dal.consultation.client.DaoClient;
import entites.Client;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped

public class C100_ContConsClient {
    
    @Inject private DaoClient dao;
    
    private Long       numcli;
    private Client    client;
    
    public void rechercher(){ client= dao.getLeClient(numcli);}

    public DaoClient getDao() {
        return dao;
    }

    public void setDao(DaoClient dao) {
        this.dao = dao;
    }

    public Long getNumcli() {
        return numcli;
    }

    public void setNumcli(Long numcli) {
        this.numcli = numcli;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
    
}